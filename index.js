// When true, moving the mouse draws on the canvas
var undoArray = new Array();
var redoArray = new Array();
var step = 0;
var bstep;
var color = "#0000ff";
var textbox;
var type;
var mousePressed = false;
var x = 0,y = 0;
var rect;
var store;
var lastX, lastY;
var m, c;
var cPushArray = new Array();
var cStep = -1;

//為了實現canvas的redo/undo功能，需保存canvas的快照（透過canvas的toDataURL來完成）
//將每一個快照都儲存在一個數據組cPushArray中，當我們每次往canvas中繪製線條时，都使用一個cPush方法来往數據組中存取快照。
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { 
        cPushArray.length = cStep; 
    }
    cPushArray.push(c.getImageData(0, 0, m.width, m.height));
    
}
//Undo的function
//點擊左兔子按鈕時，cUndo的function將啟用，並使用drawImage顯示前一個繪圖狀態
function undo() {
    if (cStep > 0) {
        cStep--;
        console.log(cStep);
        var canvasPic;
        canvasPic = cPushArray[cStep];
        c.putImageData(canvasPic, 0, 0); 
        textbox.style.visibility = 'hidden';
        /*document.title = cStep + ":" + cPushArray.length;*/
    }
}
//Redo的function
//點擊右兔子按钮時，cRedo的function將啟用，並使用drawImage顯示在cPushArray數據中的下一個可用的繪圖狀態
function redo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic;
        canvasPic = cPushArray[cStep];
        c.putImageData(canvasPic, 0, 0);
    }
    textbox.style.visibility = 'hidden';
}




//滑鼠繪畫部分
var draw; //是否在繪圖狀態
var m, c; //繪圖物件
function init() {
    m = document.getElementById("m"); //取得畫布物件參考
    c = m.getContext("2d"); //建立2d繪圖物件
}
function md() {
    c.moveTo(event.offsetX, event.offsetY); //起點
    draw = true; //進入繪圖模式
    c.beginPath(); //本次繪圖筆畫開始
}
function mv() {
    if (draw) {
        c.lineTo(event.offsetX, event.offsetY); //下一點
        c.stroke(); //繪圖
    }
}
function mup() {
    draw = false; //離開繪圖模式
    c.closePath(); //繪圖筆畫結束
    cPush();
}
window.addEventListener("load", function(event) {
    m = document.getElementById('m');//取得畫布物件參考
    c = m.getContext('2d');//建立2d繪圖物件
    
    rect = m.getBoundingClientRect();
    textbox = document.getElementById('textbox');
    textbox.style.visibility = 'hidden';
    c.getImageData(0, 0, m.width, m.height);
   
    
    document.getElementById("brush").onclick = function(){ 
        type = "brush";
        console.log(type);
    };

    document.getElementById("eraser").onclick = function(){
        type = "eraser";
        console.log(type);
    };

    document.getElementById("text").onclick=function(){ 
        type = "text";
    }
    
    document.getElementById("triangle").onclick=function(){ 
        type = "triangle";
    }

    document.getElementById("circle").onclick=function(){ 
        type = "circle";
    }

    document.getElementById("rectangle").onclick=function(){ 
        type = "rectangle";
    }
    
    document.getElementById("upload").onclick=function(){ 
        document.getElementById('upload').onchange = function(e) {
            var img = new Image();
            img.onload = draw;
            img.onerror = failed;
            img.src = URL.createObjectURL(this.files[0]);
            textbox.style.visibility = 'hidden';
        };
    }


    document.getElementById("download").onclick=function(){ 
        console.log("download");
        var link = document.createElement('a');
        link.download = 'filename.png';
        link.href = document.getElementById('m').toDataURL()
        link.click();
        textbox.style.visibility = 'hidden';
    }

    

    document.getElementById("refresh").onclick=function(){
        textbox.style.visibility = 'hidden';
        c.clearRect(0, 0, m.width, m.height);
        cPush();
        textbox.style.visibility = 'hidden';
    }
    

    m.addEventListener("mousedown", function(e){
        if(type == "brush" || type == "eraser"){
            x = e.clientX - rect.left;
            y = e.clientY - rect.top;
            mousePressed = true;
            textbox.style.visibility = 'hidden';
        }
        else if(type == "circle"){
            store=c.getImageData(0, 0, m.width, m.height);
            originalx = e.clientX - rect.left;
            originaly = e.clientY - rect.top;
            mousePressed = true;
            textbox.style.visibility = 'hidden';
            c.globalCompositeOperation='source-over';
        }
        else if(type == "rectangle"){
            console.log("mousedown");
            store=c.getImageData(0, 0, m.width, m.height);
            originalx = e.clientX - rect.left;
            originaly = e.clientY - rect.top;
            mousePressed = true;
            textbox.style.visibility = 'hidden';
            c.globalCompositeOperation='source-over';
        }
        else if(type == "triangle"){
            console.log("mousedown");
            store=c.getImageData(0, 0, m.width, m.height);
            originalx = e.clientX - rect.left;
            originaly = e.clientY - rect.top;
            mousePressed = true;
            textbox.style.visibility = 'hidden';
            c.globalCompositeOperation='source-over';
        }
        else if(type == "text"){
            console.log("mousedown");
            originalx = e.clientX - rect.left;
            originaly = e.clientY - rect.top;
            store=c.getImageData(0, 0, m.width, m.height);
            textbox.style.visibility = 'visible';

            textbox.onkeydown = function(e) {
                c.putImageData(store, 0, 0);
                c.globalCompositeOperation='source-over';
                c.font = 5*document.getElementById("selWidth").value + "px " + document.getElementById("selfont").value;
                c.fillStyle = document.getElementById("colorWell").value.toString();;
                c.fillText(textbox.value, originalx, originaly);
            }
        }
    });

    m.addEventListener("mousemove", function(e){
        if(type == "brush"){
            if (mousePressed) {
                Draw(c, x, y, e.clientX - rect.left, e.clientY - rect.top);
                //console.log(flag+" in brush");
                x = e.clientX - rect.left;
                y = e.clientY - rect.top;
            }
            $("canvas#m").css('cursor', 'url(picture04.jpg), auto'); 
        }
        else if(type == "eraser"){
            if (mousePressed) {
                _Draw(c, x, y, e.clientX - rect.left, e.clientY - rect.top);
                //console.log(flag+" in brush");
                x = e.clientX - rect.left;
                y = e.clientY - rect.top;
            }
            $("canvas#m").css('cursor', 'url(erasers.png), auto'); 
        }
        else if(type == "text"){
            $("canvas#m").css('cursor', 'text'); 
        }
        else if(type == "circle"){
            if (mousePressed === true) {
                c.putImageData(store, 0, 0);
                console.log(store);
                drawcircle(c, originalx, originaly, e.clientX - rect.left, e.clientY - rect.top);
            }
            $("canvas#m").css('cursor', 'url(circle.png), auto'); 
        }
        else if(type == "rectangle"){
            if (mousePressed === true) {
                console.log("mousemove");
                c.putImageData(store, 0, 0);
                drawrectangle(c, originalx, originaly, e.clientX-rect.left, e.clientY-rect.top);
            }
            $("canvas#m").css('cursor', 'url(rectangles.png), auto'); 
        }
        else if(type == "triangle"){
            if (mousePressed === true) {
                console.log("mousemove");
                c.putImageData(store, 0, 0);
                drawtriangle(c, originalx, originaly, e.clientX-rect.left, e.clientY-rect.top);
            }
            $("canvas#m").css('cursor', 'url(triangles.png), auto'); 
        };
    });

    m.addEventListener("mouseup", function(e){
        if(type == "brush"){
            if(mousePressed){
                Draw(c, x, y, e.clientX - rect.left, e.clientY - rect.top);
                x = 0;
                y = 0;
                mousePressed = false;
            }
        }
        else if(type == "eraser"){
            if(mousePressed){
                _Draw(c, x, y, e.clientX - rect.left, e.clientY - rect.top);
                x = 0;
                y = 0;
                mousePressed = false;
            }
        }
        else if(type == "circle"){
            if (mousePressed === true){
                console.log("mouseup");
                drawcircle(c, originalx, originaly, e.clientX - rect.left, e.clientY - rect.top);
                originalx = 0;
                originaly = 0;
                mousePressed = false;
                store=c.getImageData(0, 0, m.width, m.height);
            }
        }
        else if(type == "rectangle"){
            if (mousePressed === true) {
                console.log("mouseup");
                drawrectangle(c, originalx, originaly, e.clientX-rect.left, e.clientY-rect.top);
                originalx = 0;
                originaly = 0;
                mousePressed = false;
                store=c.getImageData(0, 0, m.width, m.height);
            }
        }
        else if(type == "triangle"){
            if (mousePressed === true) {
                console.log("mouseup");
                drawtriangle(c, originalx, originaly, e.clientX-rect.left, e.clientY-rect.top);
                originalx = 0;
                originaly = 0;
                mousePressed = false;
                store=c.getImageData(0, 0, m.width, m.height);
            }
        }
        cPush();
    });
});

function Draw(c, x1, y1, x2, y2) {
    c.beginPath();
    console.log(color);
    c.strokeStyle = document.getElementById("colorWell").value.toString();
    c.globalCompositeOperation='source-over';
    c.lineWidth = $('#selWidth').val();

    c.moveTo(x1, y1);
    c.lineTo(x2, y2);
    c.stroke();
    c.closePath();
}


function _Draw(c, x1, y1, x2, y2) {
    c.beginPath();
    c.globalCompositeOperation='destination-out';
    c.lineWidth = $('#selWidth').val();
    
    c.moveTo(x1, y1);
    c.lineTo(x2, y2);
    c.stroke();
    c.closePath();
}

function drawcircle(c, x1, y1, x2, y2) {
    c.beginPath();
    c.strokeStyle = document.getElementById("colorWell").value.toString();;
    c.lineWidth = $('#selWidth').val();;
    c.arc(x1, y1, Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)), 0, Math.PI * 2, true);
    c.stroke();
    c.closePath();
}

function drawrectangle(c, x1, y1, x2, y2) {
    c.beginPath();
    c.strokeStyle = document.getElementById("colorWell").value.toString();;
    c.lineWidth = $('#selWidth').val();;
    c.rect(x1, y1, x2-x1, y2-y1);
    c.stroke();
    
}

function drawtriangle(c, x1, y1, x2, y2) {
    c.beginPath();
    console.log(color);
    c.lineWidth = 1;
    c.moveTo(x1,y1);
    c.lineTo(x2,y2);
    c.lineTo(x1,y2);
    c.fillStyle = document.getElementById("colorWell").value.toString();;
    c.fill();
}

